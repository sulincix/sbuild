# sbuild Single binary builder

## Rules:
 * line starts with **///**
 * line syntax is **[command]** **[name]** **[variable]**
 * sbuild only use main.c file
 
## commands

### new : define new name
Variables: static library binary

### add : add C file or compiler arg
Variables: filename or gcc argument

### use : use library
Variables: pkg-config libraries

### import :  import file
Variables: filename
