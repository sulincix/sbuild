/// new sbuild static
/// add sbuild main.c
/// add sbuild -fPIC

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXBIN 1024
int curbin = 0;
char* substr(char* str, int min, int max);
void process(char* cmd, char* bin, char* var);
int compile();
int import(char* file);

char CC[100];

struct bin{
    char name[50];
    char cmd[102400];
};

struct bin b[MAXBIN];

int main(){
    if(getenv("CC")){
      strcpy(CC,getenv("CC"));
    }else{
      strcpy(CC,"gcc");
    }
    import("main.c");
    return compile();
}

int import(char* file){
    FILE *f;
    if ((f = fopen(file,"r"))== NULL){
            fprintf(stderr,"Failed to open file: %s\n",file);
            return 1;
    }
    char *line = malloc((sizeof(char)*1024)+1);
    while(!feof(f)){
        strcpy(line,"");
        fscanf(f," %[^\n]",line);
        if(line[0] == '/' && line[1] == '/' && line[2] == '/'){
            strcpy(line,substr(line,4,-1));
            char cmd[10];
            char name[100];
            char var[1024];
            sscanf(line,"%s %s %[^\n]",cmd,name,var);
            process(cmd,name,var);
        }else {
            break;
        }
    }
}

void process(char* cmd, char* bin, char* var){
    int hasbin = 1;
    if(!strcmp(cmd,"new")){
        strcpy(b[curbin].name,bin);
        strcpy(b[curbin].cmd,CC);
        strcat(b[curbin].cmd," -o ");
        strcat(b[curbin].cmd,bin);
        strcat(b[curbin].cmd," ");
        if(!strcmp(var,"static")){
            strcat(b[curbin].cmd,"-static ");
        }else if(!strcmp(var,"library")){
            strcat(b[curbin].cmd,"-shared ");
        }
        curbin++;

    }else if(!strcmp(cmd,"import")){
              import(bin);
            }
    for(int i=0;i<curbin;i++){
        if(!strcmp(b[i].name,bin)){
            hasbin = 0;
            if(!strcmp(cmd,"add")){
               strcat(b[i].cmd," ");
               strcat(b[i].cmd,var);
            }else if(!strcmp(cmd,"use")){
               strcat(b[i].cmd," `pkg-config --cflags --libs ");
               strcat(b[i].cmd,var);
               strcat(b[i].cmd,"` ");
            }
        }
    }
}

int compile(){
    int ret = 0;
    for(int i=0;i<curbin;i++){
        printf("Running: %s\n",b[i].cmd);
        ret = system(b[i].cmd);
        if (ret != 0){
          return ret;
        }
    }
    return ret;
}

char* substr(char* str, int min, int max){
    if(max == -1){
        max = strlen(str);
    }
    char* ret = malloc(sizeof(char)*(max-min+1));
    for(int i=min;i<max;i++){
        ret[i-min] = str[i];
    }
    ret[max] = '\0';
    return ret;
}
