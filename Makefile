build:
	gcc main.c -o sbuild -static
install:
	mkdir -p $(DESTDIR)/bin/ || true
	install sbuild $(DESTDIR)/bin/
clean:
	rm -f sbuild
